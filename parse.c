/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

//
// Created by Yaokai Liu on 2022/6/18.
//

#include "parse.h"
#include "message.h"
#include <stdlib.h>

Report * detect(void *sources, RulePool rule_table) {
    // sources to parse items
    void * tp = sources;
    size_t cost = 0, e_cost = 0;
    Rule rule = NULL;
    // list matched rules
    for(size_t i = 0; i < rule_table->size; i++){
        Rule e_rule = rule_table->pool[i];
        e_cost = e_rule->detector(tp);
        if (e_cost > cost){
            rule = e_rule;
            cost = e_cost;
        }
    }
    Report * report = calloc(1, sizeof(Report));
    report->cost_size = cost;
    report->applied_rule = rule;
    if (rule != NULL) {
        if (rule->kind == ignore_rule) {
            report->message = matched_ignore;
            report->result = NULL;
        }
        else {
            report->message = detect_succeed;
            report->result = rule->constructor(tp, cost);
        }
    } else {
        report->message = no_match_error;
        report->result = NULL;
    }
    return report;
}

void delReport(Report * report){
    free(report);
}

typedef struct _LinkNode LinkNode;

typedef struct _LinkNode {
    void * object;
    LinkNode * next;
} LinkNode;

LinkNode * newNode();
void delNode(LinkNode * node);

Report * parse(void * sources, RulePool rule_table) {
    // sources to parse items
    void * sp = sources;
    LinkNode * head = newNode();
    LinkNode * hp = head;
    size_t cost = 0, e_cost = 0, r_count = 0;
    Rule rule = NULL;
    // list matched rules
    while (rule_table->nonend(sp)){

        cost = 0;
        // detect which rule will be applied
        for (size_t i = 0; i < rule_table->size; i++) {
            Rule e_rule = rule_table->pool[i];
            e_cost = e_rule->detector(sp);
            // applied principle:
            //     1. maximum match: choice the rule matched most cost
            //     2. first match: choice the first rule of all most-matched rules
            // if applied principle have to be modified, just change the
            // conditional statement
            if ( e_cost > cost ) {
                rule = e_rule;
                cost = e_cost;
            }
        }
        // if 'cost' is 0, means no rule applied, means syntax error, then break loop
        if (cost == 0)
            break;

        // else means rule applied successfully
        // if rule not have to ignored, just store result object
        if (rule->kind != ignore_rule) {
            hp->object = rule->constructor(sp, cost);
            hp->next = newNode();
            hp = hp->next; // move to next
            r_count ++;
        }
        // update state
        sp = sp + cost;
    }

    Report * report = calloc(1, sizeof(Report));
    report->applied_rule = rule; // when cost
    if (cost == 0) {
        report->cost_size = cost;
        report->i_start = sp - sources;
        report->i_end = sp + abs_int(cost) - sources;
        report->message = no_match_error;
        report->result = NULL;
    } else {
        void ** res_ctr = (void **) calloc(r_count + 1, sizeof(void *));
        void ** rp = res_ctr;
        hp = head;
        while(hp->next != NULL){
            *rp = hp->object;
            rp ++; hp = hp->next;
        }
        *rp = NULL; // results should be ended with NULL

        report->i_start = 0;
        report->i_end = sp - sources;
        report->cost_size = sp - sources;
        report->message = parse_succeed;
        report->result = res_ctr;
    }
    // clean parse results
    delNode(head);

    return report;
}

LinkNode * newNode(){
   LinkNode * node = (LinkNode *) calloc(1, sizeof(LinkNode));
   node->next = NULL;
   return node;
}

void delNode(LinkNode * node){
    if (node->next != NULL)
        delNode(node->next);
    free(node);
}
