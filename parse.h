/*
MIT License

Copyright (c) 2022 Yaokai Liu

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

//
// Created by Yaokai Liu on 2022/6/18.
//

#ifndef PARSER_E_DETECT_H
#define PARSER_E_DETECT_H

#include "rule.h"

// Report type of ParserE.
// An instance of Report holds a lot of information
// from parsing, it helps debug.
typedef struct {
    // the index of the item when starting latest parse time
    size_t i_start;

    // the index of the item when ending the latest parse time
    size_t i_end;

    // how many item has been parsed for latest parse time
    size_t cost_size;

    // the parse result report message
    const char * message;

    // which rule has been applied
    Rule applied_rule;

    // the finally
    void * result;
} Report;

Report * detect(void *sources, RulePool rule_table);
Report * parse(void * sources, RulePool rule_table);

void delReport(Report *);

#endif //PARSER_E_DETECT_H
